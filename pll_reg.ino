#include <Adafruit_NeoPixel.h>
#include <SPI.h>
//Add your own path here
#include "C://Users/Trans/Documents/40MHzBand4k.h"
#include "C://Users/Trans/Documents/160MHzBand4k.h"
#include "C://Users/Trans/Documents/640MHzBand4k.h"
//Declarations
//-----------------------------------------------------------------------------------/
//LED Strip Object
Adafruit_NeoPixel strip(5,5,NEO_GRB+NEO_KHZ800);
// Software SPI bus
// colors
uint32_t red=strip.Color(50,0,0);
uint32_t green=strip.Color(0,50,0);
uint32_t blue=strip.Color(0,0,50);
uint32_t off=strip.Color(0,0,0);
//-----------------------------------------------------------------------------------/
//Structre that holds pll register values 
typedef struct
{
  unsigned int address; /* 16-bit register address */
  unsigned char value; /* 8-bit register data */
} si5344_t;
si5344_t revd_registers[700];// register struct
// defined as global
//-----------------------------------------------------------------------------------/
//setup runs once when ItsyBitsy is turned on
//I'm just using it as the main()
void setup() {
//PIN IO--------------------------//  
// switches
pinMode(A2,INPUT);
pinMode(A3,INPUT);
pinMode(A4,INPUT);
pinMode(A5,INPUT);
// buttons
pinMode(0,INPUT);
pinMode(1,INPUT);

pinMode(12,OUTPUT);
pinMode(11,OUTPUT);
pinMode(10,OUTPUT);
digitalWrite(12,HIGH);// pll reset
digitalWrite(11,LOW); // pll clk sel 1
digitalWrite(10,HIGH); // pll clk sel 0
pinMode(2,OUTPUT); // SPI Chip Select
digitalWrite(2,HIGH);
//--------------------------------------//
// Initialize NeoPixel LED STRIP
strip.begin();
strip.show();
strip.setBrightness(25);

// Register Address settings
char page;
char add;
unsigned char COMMAND;
uint8_t data [2];
/////////////

char sw_conf; // Switch Configuration variable
// clock settings 
const char clk_160 = 0X01;
const char clk_640 = 0X02;
const char clk_40 = 0X04;


// Wait to program until both buttons are pressed
// safeguard agaist accidental programming of PLL
int Buttons;//switches
// buttons
int BT[2];
int NUM_REG; // number of registers in Header File
while (1){

// Set strip to ready to program (BLUE)
setColor(blue);
// This represents switch configuration used for choosing pll settings
sw_conf=(0X0F)&((digitalRead(A5)<<3)|(digitalRead(A4)<<2)|(digitalRead(A3)<<1)|(digitalRead(A2)<<0));
// Buttons used to tell device to program
Buttons = 1;
// wait until both buttons are pressed to do anything
// Low when pressed
while (Buttons){
  BT[0] = digitalRead(0);
  BT[1] = digitalRead(1);
  Buttons=BT[0]|BT[1];
  delay(100);
}

// now assign correct config registers and program
switch (sw_conf){
  case clk_160:
    NUM_REG=SI5344_REVD_REG_CONFIG_NUM_REGS_160;
    for (int i=0;i<NUM_REG;i++){
      revd_registers[i].address=si5344_revd_registers_160[i].address;
      revd_registers[i].value=si5344_revd_registers_160[i].value;
          }
      configure_reg(NUM_REG);
      delay(3000); // prevents continual reprograming from not taking hand off of buttons
      // now wait to see if we want to reprogram it
      Buttons = 1;
      while (Buttons){
        delay(100);
        BT[0] = digitalRead(0);
        BT[1] = digitalRead(1);
        Buttons=BT[0]|BT[1];
      }
      break;
   case clk_40:
    NUM_REG=SI5344_REVD_REG_CONFIG_NUM_REGS_40;
    for (int i=0;i<NUM_REG;i++){
      revd_registers[i].address=si5344_revd_registers_40[i].address;
      revd_registers[i].value=si5344_revd_registers_40[i].value;
    }
     configure_reg(NUM_REG);
     delay(3000);
     // now wait to see if we want to reprogram it
      Buttons = 1;
      while (Buttons){
        delay(100);
        BT[0] = digitalRead(0);
        BT[1] = digitalRead(1);
        Buttons=BT[0]|BT[1];
      }
     break;
    case clk_640:
    NUM_REG=SI5344_REVD_REG_CONFIG_NUM_REGS_640;
    for (int i=0;i<NUM_REG;i++){
      revd_registers[i].address=si5344_revd_registers_640[i].address;
      revd_registers[i].value=si5344_revd_registers_640[i].value;
          }
      configure_reg(NUM_REG);
      delay(3000);
      // now wait to see if we want to reprogram it
      Buttons = 1;
      while (Buttons){
        delay(100);
        BT[0] = digitalRead(0);
        BT[1] = digitalRead(1);
        Buttons=BT[0]|BT[1];
      }
    break;
    
    default:
    
    rbg();
    delay(3000);
    // if not one of the possible configurations, show one LED of each color
    // now wait to see if we want to reprogram it. the delay is so we don't immediately trigger a reset of the process
      Buttons = 1;
      while (Buttons){
        delay(100);
        BT[0] = digitalRead(0);
        BT[1] = digitalRead(1);
        Buttons=BT[0]|BT[1];
        
      }
      break;
}

delay(5000); // so we have time to take hand off of button...you get the idea
}
}


void loop(){}// just has to be here for some reason
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Helper Functions
// set color of neopixel LED strip (The ones that work anyway)

void rbg(){
  uint32_t color_list[3] ={red,green,blue};
  strip.clear();
  strip.show();
  for (int i=0;i<4;i++) strip.setPixelColor(i,color_list[i]);
  strip.show();
}
void setColor(uint32_t color){
    strip.clear();
    strip.show();
    for (int i=0;i<5;i++) {
      if (i!=3) strip.setPixelColor(i,color);
    }
    strip.show();
}
// uses SPI to configure regsiters
// a real ugly mess
void configure_reg(int NUM_REG){
  char flag;
  flag = 0; // error flag
  char page;
  char add;
  unsigned char COMMAND;
  uint8_t data [2];
  int SelfClear[10]={0X001C,0X0514,0X0520,0X0388,0X030C,0X023F,0X0230,0X00E4,0X001E,0X001D};
  char isSelfClearing=0;
  char set_address=0X00; // commands for accessing registers
  char write_data=0X40;
  char read_data=0X80; 


SPI.begin();
SPI.beginTransaction(SPISettings(1000000,MSBFIRST,SPI_MODE0));
digitalWrite(2,LOW);// CS LOW
delay(100);
// begin data transfer !!
for (int in =0;in<NUM_REG;in++){
  COMMAND = revd_registers[in].value;
  // address is 16 bit word. the 8 msb are page
  // 8 lsb are address on page
  page = (revd_registers[in].address>>8)&0XFF;
  add = revd_registers[in].address&0XFF;
  // Set Page
  data[0]=(set_address);
  data[1]=0X01;
  SPI.transfer(data,2);
  // This set address to 0X01, the set page register address
  // Now write the new page to the register
  data[0] = write_data;
  data[1]= (page&0XFF);
  SPI.transfer(data,2);
  // Now we are on the right page 
  // we must again choose our address
  //send command
  data[0] = set_address;
  data[1] = (add&0XFF);
  SPI.transfer(data,2);
  // now we can send the actual configuration to the selected register
  data[0] = write_data;
  data[1] = (COMMAND&0XFF);
  SPI.transfer(data,2);
  // done. we can check the write by reading the 
  // value of the register
  // The verification part doesn't yet work
  
  data[0] = read_data;
  isSelfClearing=0;
  for (int z=0;z<10;z++){
  if (revd_registers[in].address==SelfClear[z]) {
    isSelfClearing=1;
    }
  }

  if (!isSelfClearing){
  SPI.transfer(data,2);
  if ((data[1]!= revd_registers[in].value)) flag=0X01; // did a stupid
  }
 }


  digitalWrite(2,HIGH);// CS HIGH
  delay(100);
  SPI.endTransaction();
  SPI.end();
  // configuration done
  if (flag=0) {
    strip.clear();
    setColor(green);// if all registers matched 
     
  }
   else {
    strip.clear();
    setColor(red);
  }
}
